#!/bin/bash 

red=`tput setaf 1`
green=`tput setaf 2`
orange=`tput setaf 3`
reset=`tput sgr0`

usage(){ 
    echo "\n${green}GaboX install script${reset}\n"
    echo "Usage:  ./install.sh [arguments] [options]\n" 
    echo "\t-d\t--db-name\t\t\tMySQL Database name"
    echo "\t-u\t--db-user\t\t\tDatabase username "
    echo "\t-w\t--db-password\t\t\tDatabase password "
    echo "\t-a\t--bo-user\t\t\tAdmin E-mail"
    echo "\t-n\t--project-name\t\t\tProject directory "
    echo "\nOptional arguments"
    echo "\t-p\t--bo-password\t\t\tAdmin password "
    echo "\t-k\t--hash-key\t\t\tHash key for password encryption "
    echo "\t-f\t--force\t\t\t\tDeletes existing files and databases before installing" 
    echo "\t-q\t--quiet\t\t\t\tNo verbose"
    echo "\t-i\t--install-demo\t\t\t\tInstall demo website"
    echo "\t-e\t--env\t\t\t\tEnvironement (dev/preprod/prod)"
    echo "\n"
} 

install_gabox(){ 

    [ -z "$quiet" ] && echo "\n${green}Installation...${reset}"
 
    if [ -w `pwd` ]; then echo -n""; 
    else 
        echo "${red}Current dir looks to be NOT WRITABLE, do you need sudo ?${reset}\n"; 
        exit 1;
    fi
   
    if [ -z "$force" ]; then echo -n"";
    else 
        [ -z "$quiet" ] && echo "${orange}Force mode: Deleting existing files and databases ${reset}"
        rm -rf gabox
        rm -rf usr
        mysql -u $DB_USER -p$DB_PASS  -e "DROP DATABASE \`$DB_NAME\`" 2>/dev/null 
    fi  
    
    EXIST=`mysql -u"$DB_USER" -p"$DB_PASS" -e "SHOW DATABASES"  2>/dev/null | grep "$DB_NAME"`;

    if [ "$EXIST" = "$DB_NAME" ]; 
    then 
        [ -z "$quiet" ] && echo "${orange}Warning ! Database ${DB_NAME} Already exits !  ${reset}"
            #exit 1
    else
      [ -z "$quiet" ] &&   echo "${green}Create database${reset}"
        mysql -u $DB_USER -p$DB_PASS -e "CREATE DATABASE IF NOT EXISTS  \`${DB_NAME}\` ; GRANT ALL PRIVILEGES ON  \`${DB_USER}\` . * TO  '${DB_USER}'@'localhost';" 2>/dev/null
    fi
    
    BO_PASS_MD5=`printf '%s' "$HASH_KEY$BO_PASS$BO_USER" | md5sum | cut -d " " -f1`
 
    [ -z "$quiet" ] &&   echo "${green}Cloning from Git${reset}"

    git  clone --quiet https://gitlab.com/n-pestana/gabox.git gabox

    [ -z "$quiet" ] &&   echo "${green}Creating project structure${reset}"



    #	├── bin
    #	├── classes
    #	│   └── auto
    #	├── etc
    #	├── sql
    #	├── static
    #	├── var
    #	│   ├── batchs
    #	│   ├── cache
    #	│   │   ├── db
    #	│   │   └── img
    #	│   ├── log
    #	│   └── upload
    #	│       └── data
    #	└── www
    #	    ├── ajax
    #	    ├── controller
    #	    ├── media -> ../var/upload/data
    #	    ├── static -> ../static/
    #	    └── views

    mkdir -p "usr/$PROJECT_NAME/bin/"
    mkdir -p "usr/$PROJECT_NAME/classes/auto/"
    mkdir -p "usr/$PROJECT_NAME/etc/"
    mkdir -p "usr/$PROJECT_NAME/sql/"
    mkdir -p "usr/$PROJECT_NAME/sql/"
    mkdir -p "usr/$PROJECT_NAME/static/"
    mkdir -p "usr/$PROJECT_NAME/var/batch/"
    mkdir -p "usr/$PROJECT_NAME/var/cache/db/"
    mkdir -p "usr/$PROJECT_NAME/var/cache/img/"
    mkdir -p "usr/$PROJECT_NAME/var/img/"
    mkdir -p "usr/$PROJECT_NAME/var/log/upload/data/"
    mkdir -p "usr/$PROJECT_NAME/var/upload/data/files/"
    mkdir -p "usr/$PROJECT_NAME/www/controller/"
    mkdir -p "usr/$PROJECT_NAME/www/views/"
    touch  "usr/$PROJECT_NAME/classes/auto/include.php"
    ln -s ../var/upload/data usr/$PROJECT_NAME/www/media
    ln -s ../static usr/$PROJECT_NAME/www/static

    chmod 0777 -R "usr/$PROJECT_NAME/var/cache/"
    chmod 0777 -R "usr/$PROJECT_NAME/var"
    chmod 0777 -R "usr/$PROJECT_NAME/classes/auto/"

    echo "<?php #main controller "          > "usr/$PROJECT_NAME/www/index.php"
    echo "Homepage"                         > "usr/$PROJECT_NAME/www/controller/index.php"
    echo "<header>header</header>"          > "usr/$PROJECT_NAME/www/header.php"
    echo "<footer>footer</footer>"          > "usr/$PROJECT_NAME/www/footer.php"
    echo "<?php include('../../../gabox/autoprepend.php');"   > "usr/$PROJECT_NAME/bin/test.php"
    echo "<?php"                            >  "usr/$PROJECT_NAME/etc/db-config.php"
    echo "\$host       = 'localhost';"      >>  "usr/$PROJECT_NAME/etc/db-config.php"
    echo "\$username   = '$DB_USER';"       >>  "usr/$PROJECT_NAME/etc/db-config.php"
    echo "\$password   = '$DB_PASS';"       >>  "usr/$PROJECT_NAME/etc/db-config.php"
    echo "\$dbname     = '$DB_NAME';"       >>  "usr/$PROJECT_NAME/etc/db-config.php"
    echo "\$charset    = 'utf8';"           >>  "usr/$PROJECT_NAME/etc/db-config.php"
    echo "\$type       = 'mysqli';"         >>  "usr/$PROJECT_NAME/etc/db-config.php"
    echo "define('PWD_HASH','$HASH_KEY');"  >>  "usr/$PROJECT_NAME/etc/db-config.php"
    echo "<?php \$objects=array();" >> "usr/$PROJECT_NAME/classes/auto/include.php"

    [ -z "$quiet" ] &&   echo "${green}Loading fixtures${reset}"
    mysql -u $DB_USER -p$DB_PASS -D $DB_NAME< gabox/sql/start.sql 2>/dev/null 
    mysql -u $DB_USER -p$DB_PASS -D $DB_NAME< gabox/sql/fixtures.sql 2>/dev/null


    if [ -z "$demo" ]; then echo -n"";
    else
        [ -z "$quiet" ] &&   echo "${green}Loading DEMO datas${reset}"
        git  clone --quiet --recursive https://gitlab.com/n-pestana/gabox-demo-datas.git gabox-demo-datas 
        mysql -u $DB_USER -p$DB_PASS -D $DB_NAME < gabox-demo-datas/sql/start.sql 
        sh  gabox-demo-datas/install.sh 2>/dev/null
        cp -R gabox-demo-datas/www/ usr/$PROJECT_NAME/
    fi
    
    
    mysql -u $DB_USER -p$DB_PASS -D $DB_NAME -e "INSERT INTO gen_admins (id_admins,is_valid,email,password,firstname,lastname,usertype,id_locales) VALUES (1, 1, '$BO_USER', '$BO_PASS_MD5', 'Demo', 'Demo', 1, 4);"  2>/dev/null 
    [ -z "$quiet" ] &&   echo "\nInstallation ${green}OK${reset} ! here your access : \n"
    [ -z "$quiet" ] &&   echo "Backoffice login:\t\t$BO_USER"
    [ -z "$quiet" ] &&  echo "Backoffice Password : \t\t$BO_PASS"
    [ -z "$quiet" ] &&  echo "\n"

    cd gabox
    composer install -n
    composer dump-autoload -n
    cd ..
}

while [ "$1" != "" ]; do
    case $1 in
        -d | --db-name)         shift
                                DB_NAME=$1
                                ;;
        -u | --db-user)         shift
                                DB_USER=$1
                                ;;
        -w | --db-password)     shift
                                DB_PASS=$1
                                ;;
        -a | --bo-user)         shift
                                BO_USER=$1
                                ;;
        -p | --bo-password)     shift
                                BO_PASS=$1
                                ;;
        -k | --hash-key)        shift
                                HASH_KEY==$1
                                ;;
        -n | --project-name)    shift
                                PROJECT_NAME=$1
                                ;;

        -e | --env) 		shift
                                ENVIR=$1
                                ;;

        -i | --install-demo)    
                                demo=$1
                                ;;
        -f | --force)           
                                force=1
                                ;;
        -q | --quiet)           quiet=1
                                ;;

        -h | --help )           usage
                                exit
                                ;;
        * )                     #usage
                                exit 1
    esac
    shift
done

if [ -z "$DB_NAME" ]  || [ -z "$DB_USER" ] || [ -z "$DB_PASS" ] || [ -z "$BO_USER" ] || [ -z "$PROJECT_NAME" ] ; 
then

  usage
  echo  "\n${red}Error: Missing option${reset}\n"
  [ -z "$DB_NAME" ] && echo "\t-Database name  is empty -d  or --db-name" 
  [ -z "$DB_USER" ] && echo "\t-Database username  is empty -u  or --db-user"
  [ -z "$DB_PASS" ] && echo "\t-Database password is empty -w  or --db-password"
  [ -z "$BO_USER" ] && echo "\t-Backoffice login email is empty -u  or --bo-user"
  [ -z "$PROJECT_NAME" ] && echo "\t-Project directory  is empty -n  or --project-name" 
  echo "\n"

else

    # setting backoffice password if not set
    [ -z "$BO_PASS" ] && BO_PASS=`date +%s | sha256sum | base64 | head -c 8`

    # setting hash key if not set
    [ -z "$HASH_KEY" ] && HASH_KEY=`date +%s | sha256sum | base64 | head -c 12` 

    # setting environnement if not set
    [ -z "$ENVIR" ] && ENVIR='dev'

    install_gabox 

fi



